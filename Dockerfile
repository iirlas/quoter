FROM python:3.8-alpine

WORKDIR /app
RUN apk add --no-cache gcc linux-headers musl-dev
COPY requirements.txt /app
COPY . .
RUN pip install --upgrade pip && pip install --no-cache-dir -r requirements.txt

RUN apk update && apk add npm && npm i nodemon -g && nodemon -v


EXPOSE 80
#CMD ["nodemon", "bot.py -u"]
CMD nodemon -L bot.py -u
