import typing
import random
import discord
import configparser
import logging
import psycopg2
import io
import calendar
import pandas as pd
import matplotlib.pyplot as plt
import demoji
from psycopg2 import sql
from datetime import datetime, timedelta
from discord.ext import commands
from typing import List

# configuration
config_path = '.credstore/quoter-bot-config.ini'
config = configparser.ConfigParser()
config.read(config_path)

# token used to access discord API
token = ''
# postgres access information
pg_host = ''
pg_dbname = ''
pg_user = ''
pg_pass = ''

try:
    token = config['APP']['TOKEN']
    pg_host = config['DB']['HOST']
    pg_dbname = config['DB']['NAME']
    pg_user = config['DB']['USER']
    pg_pass = config['DB']['PASS']
except:
    logging.info('Failed to open config file ' + config_path + ' ... Quitting.')
    raise

# database setup
conn = None
try:
    conn = psycopg2.connect(host=pg_host, dbname=pg_dbname, user=pg_user, password=pg_pass)
except:
    logging.info('Failed to connect to database ... Quitting.')
    raise

intents = discord.Intents.default()
intents.members = True
intents.message_content = True
bot = commands.Bot(command_prefix='!', intents=intents)
# days between each entry
timescale = 7


@bot.event
async def on_connect():
    print('Connected to server!')


@bot.event
async def on_disconnect():
    print('Disconnected from server!')


@bot.event
async def on_ready():
    for guild in bot.guilds:
        with conn.cursor() as cur:
            cur.execute('select * from pg_tables where tablename = %s;', [str(guild.id)])
            if cur.rowcount <= 0:
                query = sql.SQL(
                    'create table {} (channelId bigint, userId bigint, time timestamp, id bigint not null, primary key(channelId, userId, time));').format(
                    sql.Identifier(str(guild.id))
                )
                cur.execute(query)
            for channel in guild.text_channels:
                async for message in channel.history(limit=1):
                    await bot.process_commands(message)


@bot.event
async def on_message(message):
    if message.author.bot:
        return

    if bot.user in message.mentions:
        await message.channel.send('Don\'t @ me!')

    await bot.process_commands(message)


@bot.command(name='quote-help')
async def quote_help(context):
    await context.send(f'`You can repost a quote from any user from any channel using the following:`\n\
                        `Format: !quote [User] [Channel(Optional)] [Emoji(Optional)]`\n\
                        `Examples:`\n\
                        !quote <@{context.author.id}>\n\
                        !quote <@{context.author.id}> <#{context.channel.id}>\n\
                        !quote <@{context.author.id}> <#{context.channel.id}> :+1:')


@bot.command()
async def quote_set_timescale(context, entry_range: int):
    timescale = entry_range
    await context.send(f'Cache cleared, entry range set to {timescale}')


@bot.command()
async def quote(context, user: discord.User, channel: discord.TextChannel, emoji: typing.Optional[discord.Emoji]):
    async with context.typing():
        if not user:
            await context.send('User was not found!')
            return

        if bot.user == user:
            return

        if not channel:
            await context.send('Channel was not found!')
            return

        date_start = datetime(channel.created_at.year, channel.created_at.month, channel.created_at.day)
        date_range = (datetime.today() - date_start).days // timescale
        dates = [date_start + timedelta(days=i * timescale) for i in range(date_range)]
        random.shuffle(dates)
        message = None
        for date in dates:
            if message:
                break
            new_entry = False
            with conn.cursor() as cur:
                # always cache miss on closest date
                if (date + timedelta(days=timescale)) < datetime.today():
                    # Search DB for message near date
                    query = sql.SQL(
                        'select id from {} where channelId = %s and userId = %s and time between %s and %s;').format(
                        sql.Identifier(str(context.guild.id))
                    )
                    data = (channel.id, user.id, date, date + timedelta(days=timescale))
                    cur.execute(query, data)
                    if cur.rowcount > 0:
                        message_id = random.choice(cur.fetchall())[0]
                        message = await channel.fetch_message(int(message_id))
                        continue
                    else:
                        new_entry = True
                # Search discord for message near date
                fetched = (await channel
                           .history(limit=None, after=date, before=date + timedelta(days=timescale))
                           .filter(lambda message: message.author == user)
                           .flatten())
                if len(fetched) > 0:
                    print(f'process fetch')
                    filtered_messages = [
                        message for message in fetched
                        if len(message.reactions) > 0
                        if not emoji or emoji in map(lambda reaction: reaction.emoji, message.reactions)
                    ]
                    if len(filtered_messages) > 0:
                        print(f'getting final message')
                        message = random.choice(filtered_messages)

        if not message:
            await context.send(f'Could not find anything of value from **{user}** in **{channel}**.')
        else:
            if new_entry:
                with conn.cursor() as cur:
                    query = sql.SQL('insert into {}(channelId, userId, time, id) values (%s, %s, %s, %s);').format(
                        sql.Identifier(str(context.guild.id))
                    )
                    data = (channel.id, user.id, message.created_at, message.id)
                    cur.execute(query, data)
            if len(message.attachments) > 0:
                await context.send(
                    f'**{user}**\n>>> ' + '\n'.join([attachment.url for attachment in message.attachments]))
            else:
                await context.send(f'**{user}**\n>>> {message.content}')


# TODO plot idea: user by letter, bigram
@bot.command()
async def rewind(context: commands.Context, guild_id: int, channel_id: typing.Union[int, None],
                 year: typing.Union[int, None], limit: int = 5):
    guild: discord.Guild = bot.get_guild(guild_id)
    if guild is None:
        raise AssertionError('Could not find guild')
    out_channel = guild.get_channel(channel_id)
    if out_channel is None:
        raise AssertionError('Could not find channel')
    if year is None:
        before = datetime(datetime.today().year, 1, 1)
        after = datetime(datetime.today().year - 1, 1, 1)
    else:
        before = datetime(year + 1, 1, 1)
        after = datetime(year, 1, 1)
    async with context.typing():
        # Fetch Data
        messages = []
        for channel in guild.text_channels:
            messages.extend(await channel.history(after=after, before=before, limit=None).flatten())
        messages_df = pd.DataFrame([{attr: getattr(message, attr) for attr in dir(message) if not attr.startswith('_')} for message in messages])

        # Process Data
        total_words = messages_df['content'].apply(lambda text: sum([1 for word in text.split() if word.isalnum()])).sum()
        total_reactions = messages_df['reactions'].apply(lambda reactions: sum([reaction.count for reaction in reactions])).sum()
        # User By Word
        messages_df['words'] = messages_df['content'].map(lambda content: [word.lower() for word in content.split() if word.isalnum()])
        user_by_word_df = messages_df.explode('words').groupby(['words', 'author'], sort=False).size().unstack(fill_value=0)
        with open('words_to_filter.txt', 'r') as f:
            words_to_filter = [line.rstrip().lower() for line in f]
            user_by_word_df = user_by_word_df.drop(words_to_filter, errors='ignore')
        user_by_word_df = user_by_word_df.loc[user_by_word_df.sum(axis='columns').sort_values(ascending=False).index]
        user_by_word_df = user_by_word_df[user_by_word_df.sum(axis='index').sort_values(ascending=False).index]
        user_by_word_df.columns = user_by_word_df.columns.map(lambda user: user.name)
        user_by_word_df.index.name = ''
        user_by_word_df.columns.name = ''

        # User By Emoji
        user_by_reaction_df = pd.DataFrame(columns=['user', 'reaction'])
        for reactions in messages_df['reactions']:
            for reaction in reactions:
                async for user in reaction.users():
                    user_by_reaction_df.loc[len(user_by_reaction_df.index)] = pd.Series({'user': user, 'reaction': reaction})
        user_by_reaction_df = user_by_reaction_df.groupby(['user', 'reaction'], sort=False).size().unstack(level=0, fill_value=0)
        user_by_reaction_df = user_by_reaction_df.loc[user_by_reaction_df.sum(axis='columns').sort_values(ascending=False).index]
        user_by_reaction_df = user_by_reaction_df[user_by_reaction_df.sum(axis='index').sort_values(ascending=False).index]
        top_reactors_df = user_by_reaction_df.sum(axis='index')
        user_by_reaction_df.index = user_by_reaction_df.index.map(lambda reaction: reaction.emoji.name if not isinstance(reaction.emoji, str) else demoji.replace_with_desc(reaction.emoji))
        user_by_reaction_df.columns = user_by_reaction_df.columns.map(lambda user: user.name)
        user_by_reaction_df.index.name = ''
        user_by_reaction_df.columns.name = ''

        # User Post By Day
        messages_df['day'] = messages_df['created_at'].map(lambda utc: utc.weekday())
        user_by_day_df = messages_df.groupby(['day', 'author'], sort=False).size().unstack(fill_value=0)
        user_by_day_df.index = user_by_day_df.index.map(lambda day: calendar.day_name[day])
        user_by_day_df.columns = user_by_day_df.columns.map(lambda user: user.name)
        user_by_day_df.index.name = ''
        user_by_day_df.columns.name = ''

        # Top @'ed Users
        author_by_mentioned_user_df = messages_df.explode('mentions').groupby(['mentions', 'author'], sort=False).size().unstack(fill_value=0)
        author_by_mentioned_user_df = author_by_mentioned_user_df.loc[author_by_mentioned_user_df.sum(axis='columns').sort_values(ascending=False).index]
        author_by_mentioned_user_df = author_by_mentioned_user_df[author_by_mentioned_user_df.sum(axis='index').sort_values(ascending=False).index]
        author_by_mentioned_user_df.index = author_by_mentioned_user_df.index.map(lambda user: user.name)
        author_by_mentioned_user_df.columns = author_by_mentioned_user_df.columns.map(lambda user: user.name)
        author_by_mentioned_user_df.index.name = ''
        author_by_mentioned_user_df.columns.name = ''

        messages_df['reaction_counts'] = messages_df['reactions'].map(lambda reactions: sum([reaction.count for reaction in reactions]))
        post_by_reaction_count_df = messages_df.sort_values('reaction_counts', ascending=False)['jump_url'].head(n=limit)

        top_posters_df = messages_df['author'].value_counts()

        # Display Data
        data = [user_by_word_df.head(n=limit), user_by_reaction_df.head(n=limit), user_by_day_df, author_by_mentioned_user_df.head(n=limit)]
        titles = ['Top Words', 'Top Reactions', 'Day of Posts', 'Top @ed Users']
        buffers = [io.BytesIO() for _ in range(len(data))]
        for index, item in enumerate(data):
            fig, ax = plt.subplots(1, 1, figsize=(10, 10), facecolor='#eafff5')
            fig.tight_layout(w_pad=8, h_pad=1)
            item.plot.barh(ax=ax, stacked=True)
            ax.legend()
            ax.set_title(titles[index])
            ax.invert_yaxis()
            ax.set_title('')
            plt.tight_layout()
            plt.savefig(buffers[index], format='png')
            plt.close()
            buffers[index].seek(0)
        await out_channel.send(content='It\'s Rewind Time!', files=[discord.File(fp=b, filename=f'{index}.png') for b in buffers])
        for buffer in buffers:
            buffer.close()
        await out_channel.send(f'''
        Total words written **{total_words:.0f}!**
        Total Reactions **{total_reactions:.0f}!**
        Top Poster **<@{top_posters_df.idxmax().id}>** with **{top_posters_df.max():.0f}** total messages!
        Top Reactor **<@{top_reactors_df.idxmax().id}>** with **{top_reactors_df.max():.0f}** total reactions!
        ''')
        await out_channel.send(f'Top Posts for {guild}\n' + '\n'.join(
            [f'{index + 1}. {jump_url}' for index, jump_url in enumerate(post_by_reaction_count_df)]))


bot.run(token, reconnect=True)
